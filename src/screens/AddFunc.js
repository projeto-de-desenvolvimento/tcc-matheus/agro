import React, { Component } from 'react'
import {
    Platform,
    Modal,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    TouchableWithoutFeedback
} from 'react-native'


import axios from 'axios'

import moment from 'moment'
import commonStyles from '../commonStyles.js'

const initialState = {
    name: '',
    occupation: '',
    salary: '',
    nascimento: ''
}

export default class AddFunc extends Component {

    constructor(props) {
        super(props)
    }

    state = {
        ...initialState
    }

    save = async () => {
        const newFunc = {
            name: this.state.name,
            occupation: this.state.occupation,
            salary: this.state.salary,
            nascimento: this.state.nascimento
        }

        try {
            let data = await AsyncStorage.getItem('userData')
            let a = await axios.post(`http://192.168.111.1:3000/employees`, {
                name: newFunc.name,
                occupation: newFunc.occupation,
                salary: newFunc.salary,
                nascimento: newFunc.nascimento
            }, {
                headers: {
                    'Authorization': 'Bearer ' + JSON.parse(data).token
                }
            })
            this.props.parentReference()
            this.props.onCancel()
            this.setState({ ...initialState })
        } catch (e) {
            showError(e)
        }

        this.props.onSave && this.props.onSave(newFunc)
        this.setState({ ...initialState })
    }

    render() {
        return (
            <Modal background='grey' transparent={true} visible={this.props.isVisible}
                onRequestClose={this.props.onCancel}
                animationType='slide'>
                <TouchableWithoutFeedback
                    onPress={this.props.onCancel}>
                    <View style={styles.background}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <Text style={styles.header}>Cadastro de Funcionários</Text>
                    <TextInput style={styles.input} placeholder="Nome"
                        onChangeText={name => this.setState({ name })}
                        value={this.state.name} />
                    <TextInput style={styles.input} placeholder="Ocupação"
                        onChangeText={occupation => this.setState({ occupation })}
                        value={this.state.occupation} />
                    <TextInput style={styles.input} placeholder="Salário"
                        onChangeText={salary => this.setState({ salary })}
                        value={this.state.salary} />
                    <TextInput style={styles.input} placeholder="Nascimento"
                        onChangeText={nascimento => this.setState({ nascimento })}
                        value={this.state.nascimento} />
                    {/* {this.getDatePicker()} */}
                    <View style={styles.buttons}>
                        <TouchableOpacity onPress={this.props.onCancel}>
                            <Text style={styles.button}>Cancelar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.save}>
                            <Text style={styles.button}>Salvar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableWithoutFeedback
                    onPress={this.props.onCancel}>
                    <View style={styles.background}></View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.7)'
    },
    container: {
        backgroundColor: '#FFF'
    },
    header: {
        fontFamily: commonStyles.fontFamily,
        backgroundColor: commonStyles.colors.today,
        color: commonStyles.colors.mainText,
        textAlign: 'center',
        padding: 15,
        fontSize: 18,
        fontWeight: "bold"
    },
    input: {
        fontFamily: commonStyles.fontFamily,
        backgroundColor: commonStyles.colors.text,
        height: 40,
        margin: 10,
        borderColor: 'black',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#E3E3E3',
        borderRadius: 6, 
        fontWeight: 'bold'
    },
    buttons: {
        fontFamily: commonStyles.fontFamily,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        fontWeight: "bold"
    },
    button: {
        margin: 20,
        marginTop: 200,
        marginRight: 30,
        color: commonStyles.colors.today
    },
    doneAt: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 20,
        marginLeft: 15
    }
})