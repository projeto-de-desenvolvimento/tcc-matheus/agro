import React, { Component } from 'react'
import {
    Platform,
    Modal,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
    TouchableWithoutFeedback
} from 'react-native'


import axios from 'axios'

import moment from 'moment'
import commonStyles from '../commonStyles.js'

import DateTimePicker from '@react-native-community/datetimepicker'

const initialState = {
    implement: '',
    desc: '',
    doneAt: new Date(),
    showDatePicker: false
}



export default class AddDeliverie extends Component {

    constructor(props) {
        super(props)
    }

    state = {
        ...initialState
    }

    save = async () => {
        const newMaintenance = {
            implement: this.state.implement,
            desc: this.state.desc,
            doneAt: this.state.doneAt
        }

        try {
            let data = await AsyncStorage.getItem('userData')
            let a = await axios.post(`http://192.168.111.1:3000/maintenances`, {
                implement: newMaintenance.implement,
                desc: newMaintenance.desc,
                doneAt: newMaintenance.doneAt
            }, {
                headers: {
                    'Authorization': 'Bearer ' + JSON.parse(data).token
                }
            })
            this.props.parentReference()
            this.props.onCancel()
            this.setState({ ...initialState })
        } catch (e) {
            showError(e)
        }

        this.props.onSave && this.props.onSave(newMaintenance)
        this.setState({ ...initialState })
    }

    getDatePicker = () => {
        let datePicker = (
            <DateTimePicker
                mode="date"
                value={this.state.doneAt}
                onChange={(_, doneAt) => {
                    doneAt = doneAt ? doneAt : new Date()
                    this.setState({ doneAt, showDatePicker: false })
                }}
            />
        )

        const dateString = moment(this.state.doneAt).format('ddd, D [de] MMMM [de] YYYY')

        if (Platform.OS === 'android') {
            datePicker = (
                <View>
                    <TouchableOpacity onPress={() => this.setState({ showDatePicker: true })}>
                        <Text style={styles.doneAt}>
                            {dateString}
                        </Text>
                    </TouchableOpacity>
                    {this.state.showDatePicker && datePicker}
                </View>
            )
        }

        return datePicker
    }

    render() {
        return (
            <Modal background='grey' transparent={true} visible={this.props.isVisible}
                onRequestClose={this.props.onCancel}
                animationType='slide'>
                <TouchableWithoutFeedback
                    onPress={this.props.onCancel}>
                    <View style={styles.background}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <Text style={styles.header}>Manutenção de Implementos</Text>
                    <TextInput style={styles.input} placeholder="Implemento"
                        onChangeText={implement => this.setState({ implement })}
                        value={this.state.implement} />
                    <TextInput style={styles.input} placeholder="Descrição"
                        onChangeText={desc => this.setState({ desc })}
                        value={this.state.desc} />
                    {this.getDatePicker()}
                    <View style={styles.buttons}>
                        <TouchableOpacity onPress={this.props.onCancel}>
                            <Text style={styles.button}>Cancelar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.save}>
                            <Text style={styles.button}>Salvar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableWithoutFeedback
                    onPress={this.props.onCancel}>
                    <View style={styles.background}></View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.7)'
    },
    container: {
        backgroundColor: '#FFF'
    },
    header: {
        fontFamily: commonStyles.fontFamily,
        backgroundColor: commonStyles.colors.today,
        color: commonStyles.colors.mainText,
        textAlign: 'center',
        padding: 15,
        fontSize: 18,
        fontWeight: "bold"
    },
    input: {
        fontFamily: commonStyles.fontFamily,
        height: 40,
        margin: 10,
        borderColor: 'black',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#E3E3E3',
        borderRadius: 6,
        fontWeight: 'bold'
    },
    buttons: {
        fontFamily: commonStyles.fontFamily,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        fontWeight: "bold"
    },
    button: {
        margin: 20,
        marginTop: 200,
        marginRight: 30,
        color: commonStyles.colors.today
    },
    doneAt: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 20,
        marginLeft: 15
    }
})