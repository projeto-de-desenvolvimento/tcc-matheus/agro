import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Text,
    FlatList,
    Alert
} from 'react-native'
import { PieChart } from 'react-native-svg-charts'
import commonStyles from '../commonStyles.js'
import tomorrow from '../../assets/imgs/tomorrow.jpg'

import { Icon } from 'react-native-elements'
import Icons from 'react-native-vector-icons/FontAwesome'
import moment from 'moment'
import 'moment/locale/pt-br'

import AsyncStorage from '@react-native-community/async-storage'
import { server, showError } from '../common'

import axios from 'axios'
import { DrawerActions } from 'react-navigation-drawer';
import AddPluviometer from './AddPluviometer.js'
import PluviometerComponent from '../components/PluviometerComponent'


const initialState = {
    showDonePluviometer: true,
    showAddPluviometer: false,
    visiblePluviometers: [],
    pluviometers: []
}

const colors = {
    themeColor: "#4263ec",
    white: "#fff",
    background: "#f4f6fc",
    greyish: "#a4a4a4",
    tint: "#2b49c3"
}



export default class Pluviometer extends Component {

    months = []

    state = {
        ...initialState
    }

    componentWillMount = async () => {
        this.loadPluviometer()
    }

    componentDidMount = async () => {
        const stateString = await AsyncStorage.getItem('pluviometersState')
        const savedState = JSON.parse(stateString) || initialState
        this.setState({
            showDonePluviometer: savedState.showDonePluviometer
        }, this.filterPluviometer)


        this.loadPluviometer()
        console.disableYellowBox = true
    }
    _hideModal = () => {
        console.error()
        this.loadPluviometer()
    }


    loadPluviometer = async () => {
        try {
            const res = await axios.get(`http://192.168.111.1:3000/pluviometers`)
            this.setState({ pluviometers: res.data }, this.filterPluviometer)
            this.setState({ pluviometers: await this.getPluviometers(res.data) })
        } catch (e) {
            showError(e)
        }

    }

    toggleFilter = () => {
        this.setState({ showDonePluviometer: !this.state.showDonePluviometer })
    }
    syncButton = () => {
        this.loadPluviometer()
    }

    filterPluviometer = () => {
        let visiblePluviometers = null
        if (this.state.showDonePluviometer) {
            visiblePluviometers = [...this.state.pluviometers]
        } else {
            const pending = pluviometer => pluviometer.doneAt === null
            visiblePluviometers = this.state.pluviometers.filter(pending)
        }

        this.setState({ visiblePluviometers })
        AsyncStorage.setItem('pluviometerState'), JSON.stringify({
            showDonePluviometer: this.state.showDonePluviometer
        })
    }

    togglePluviometer = async pluviometersId => {
        try {
            await axios.put(`${server}/pluviometers/${pluviometersId}/toggle`)
            this.loadPluviometer()
        } catch (e) {
            showError(e)
        }
    }

    getMonth(index) {
        if (index === 1) {
            return 'Janeiro'
        }
        if (index === 2) {
            return 'Fevereiro'
        }
        if (index === 3) {
            return 'Março'
        }
        if (index === 4) {
            return 'Abril'
        }
        if (index === 5) {
            return 'Maio'
        }
        if (index === 6) {
            return 'Junho'
        }
        if (index === 7) {
            return 'Julho'
        }
        if (index === 8) {
            return 'Agosto'
        }
        if (index === 9) {
            return 'Setembro'
        }
        if (index === 10) {
            return 'Outubro'
        }
        if (index === 11) {
            return 'Novembro'
        }
        if (index === 12) {
            return 'Dezembro'
        }

    }

    async getPluviometers(input) {
        let data = []
        this.months = []
        let lastMonth = -1
        input.forEach(async (pluvi) => {
            let month = await new Date(pluvi.doneAt).getMonth() + 1
            if (lastMonth !== month) {
                let temp = this.getMonth(month)
                this.months.push(temp)
                lastMonth = month
            }
            if (data[month] !== undefined) {
                data[month] = String(parseInt(data[month]) + parseInt(pluvi.quantityMM))
            } else {
                data[month] = pluvi.quantityMM
            }
        })
        return await Object.values(await data)
    }

    render() {
        const today = moment().locale('pt-br').format('ddd, D [de] MMMM')

        const date = doneAt ? doneAt : new Date()
        const doneAt = moment(date).locale('pt-br').format('ddd, D [de] MMMM')

        let data = this.state.pluviometers
        const randomColor = () => ('#' + ((Math.random() * 0xffffff) << 0).toString(16) + '000000').slice(0, 7)

        const pieData = this.state.pluviometers
            .map((value, index) => ({
                value,
                svg: {
                    fill: randomColor(),
                    onPress: () => {
                        Alert.alert(" Referente ao mês de " + this.months[index] +"." + "\n" +  " Contém um acumulativo de " + this.state.pluviometers[index] + "mm.")
                    },
                },
                key: `pie-${index}`,
            }))

        return (
            <View style={styles.container}>
                <AddPluviometer isVisible={this.state.showAddPluviometer}
                    onCancel={() => this.setState({ showAddPluviometer: false })}
                    parentReference={() => this.loadPluviometer()} />
                <ImageBackground source={tomorrow} style={styles.background}>
                    <TouchableOpacity >
                        <Icon raised name='reorder' type='material' size={20} style={{ color: colors.white }}
                            onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}></Icon>
                    </TouchableOpacity>
                    <View style={styles.titleBar}>
                        <Text style={styles.title}>Pluviômetro</Text>
                        <Text style={styles.subtitle}>{today}</Text>
                    </View>
                </ImageBackground>
                <View style={styles.home}>
                    <FlatList data={this.state.visiblePluviometers} keyExtractor={item => `${item.id}`}
                        renderItem={({ item }) => <PluviometerComponent {...item}
                            togglePluviometer={this.togglePluviometer} />} />
                </View>
                <View style={{ justifyContent: 'center', marginTop: -90, flex: 1 }}>
                    <PieChart style={{ height: 220, top: 70 }} data={pieData} />
                </View>
                <TouchableOpacity style={styles.addButton}
                    activeOpacity={0.9}
                    onPress={() => this.setState({ showAddPluviometer: true })}>
                    <Icons name="plus" size={20} color={commonStyles.colors.today} />
                </TouchableOpacity>
            </View >
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    },

    header: {
        fontFamily: commonStyles.fontFamily,
        backgroundColor: commonStyles.colors.today,
        color: commonStyles.colors.mainText,
        textAlign: 'center',
        padding: 15,
        fontSize: 18,
        fontWeight: "bold"
    },

    background: {
        flex: 0.5,
    },

    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },

    home: {
        flex: 0.6
    },

    title: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 40,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 20,
        fontWeight: "bold"
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 20,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 30,
        fontWeight: "bold"
    },
    addButton: {
        position: 'absolute',
        right: 30,
        bottom: 30,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
