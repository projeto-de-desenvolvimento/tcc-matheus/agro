import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    StyleSheet,
    FlatList,
    TouchableOpacity
} from 'react-native'

import tomorrow from '../../assets/imgs/tomorrow.jpg'

import { Icon } from 'react-native-elements'
import Icons from 'react-native-vector-icons/FontAwesome'

import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { server, showError } from '../common'

import moment from 'moment'
import 'moment/locale/pt-br'
import commonStyles from '../commonStyles.js'
import MaintenanceComponent from '../components/MaintenanceComponent'
import AddMaintenance from './AddMaintenance'

const initialState = {
    showDoneMaintenances: true,
    showAddMaintenance: false,
    visibleMaintenance: [],
    maintenances: []
}

const colors = {
    themeColor: "#4263ec",
    white: "#fff",
    background: "#f4f6fc",
    greyish: "#a4a4a4",
    tint: "#2b49c3"
}

export default class Maintenance extends Component {

    state = {
        ...initialState
    }

    componentDidMount = async () => {
        const stateString = await AsyncStorage.getItem('maintenancesState')
        const savedState = JSON.parse(stateString) || initialState
        this.setState({
            showDoneTasks: savedState.showDoneMaintenances
        }, this.filterMaintenances)

        this.loadMaintenances()
        console.disableYellowBox = true
    }
    _hideModal = () => {
        console.error()
        this.loadMaintenances()
    }


    loadMaintenances = async () => {
        try {
            const res = await axios.get(`http://192.168.111.1:3000/maintenances`)
            this.setState({ maintenances: res.data }, this.filterMaintenances)
        } catch (e) {
            showError(e)
        }

    }

    toggleFilter = () => {
        this.setState({ showDoneMaintenances: !this.state.showDoneMaintenances })
    }
    syncButton = () => {
        this.loadMaintenances()
    }

    filterMaintenances = () => {
        let visibleMaintenance = null
        if (this.state.showDoneMaintenances) {
            visibleMaintenance = [...this.state.maintenances]
        } else {
            const pending = maintenances => maintenances.doneAt === null
            visibleMaintenance = this.state.maintenances.filter(pending)
        }

        this.setState({ visibleMaintenance })
        AsyncStorage.setItem('maintenancesState'), JSON.stringify({
            showDoneMaintenances: this.state.showDoneMaintenances
        })
    }

    toggleMaintenances = async maintenancesId => {
        try {
            await axios.put(`${server}/maintenances/${maintenancesId}/toggle`)
            this.loadMaintenances()
        } catch (e) {
            showError(e)
        }
    }

    deleteMaintenances = async maintenancesId => {
        try {
            await axios.delete(`${server}/maintenances/${maintenancesId}`)
            this.loadMaintenances()
        } catch (e) {
            showError(e)
        }
    }


    render() {
        const today = moment().locale('pt-br').format('ddd, D [de] MMMM')
        return (
            <View style={styles.container}>
                <AddMaintenance isVisible={this.state.showAddMaintenance}
                    onCancel={() => this.setState({ showAddMaintenance: false })}
                    parentReference={() => this.loadMaintenances()}
                />
                <ImageBackground source={tomorrow} style={styles.background}>
                    <TouchableOpacity  >
                        <Icon raised name='reorder' type='material' size={20} style={{ color: colors.white }}
                            onPress={() => this.props.navigation.openDrawer()}></Icon>
                    </TouchableOpacity>
                    <View style={styles.titleBar}>
                        <Text style={styles.title}>Manutenção de Implementos</Text>
                        <Text style={styles.subtitle}>{today}</Text>
                    </View>
                </ImageBackground>
                <View style={styles.home}>
                    <FlatList data={this.state.visibleMaintenance} keyExtractor={item => `${item.id}`}
                        renderItem={({ item }) => <MaintenanceComponent {...item}
                            ontoggleMaintenances={this.toggleMaintenances} onDelete={this.deleteMaintenances} />} />
                </View>
                <TouchableOpacity style={styles.addButton}
                    activeOpacity={0.7}
                    onPress={() => this.setState({ showAddMaintenance: true })}>
                    <Icons name="plus" size={20} color={commonStyles.colors.today} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 3
    },
    home: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end',
        marginTop: 185
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 50,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 20,
        fontWeight: "bold",
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 20,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 30,
        fontWeight: "bold"
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
    },
    button3: {
        marginLeft: 15
    },
    addButton: {
        position: 'absolute',
        right: 30,
        bottom: 30,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    }
})