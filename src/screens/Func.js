import React, { Component } from 'react'
import { View, Text, ImageBackground, StyleSheet, FlatList, TouchableOpacity } from 'react-native'

import tomorrow from '../../assets/imgs/tomorrow.jpg'

import { Icon } from 'react-native-elements'
import Icons from 'react-native-vector-icons/FontAwesome'

import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { server, showError } from '../common'
import { DrawerActions } from 'react-navigation-drawer';

import moment from 'moment'
import 'moment/locale/pt-br'
import commonStyles from '../commonStyles.js'
import FuncComponent from '../components/FuncComponent.js'
import AddFunc from './AddFunc'

const initialState = {
    showDoneFuncs: true,
    showAddFuncs: false,
    visibleFuncs: [],
    funcs: []
}

const colors = {
    themeColor: "#4263ec",
    white: "#fff",
    background: "#f4f6fc",
    greyish: "#a4a4a4",
    tint: "#2b49c3"
}

export default class Func extends Component {

    state = {
        ...initialState
    }

    componentDidMount = async () => {
        const stateString = await AsyncStorage.getItem('funcsState')
        const savedState = JSON.parse(stateString) || initialState
        this.setState({
            showDoneFuncs: savedState.showDoneFuncs
        }, this.filterFuncs)

        this.loadFuncs()
        console.disableYellowBox = true
    }
    _hideModal = () => {
        console.error()
        this.loadFuncs()
    }


    loadFuncs = async () => {
        try {
            const res = await axios.get(`http://192.168.111.1:3000/employees`)
            this.setState({ funcs: res.data }, this.filterFuncs)
        } catch (e) {
            showError(e)
        }

    }

    toggleFilter = () => {
        this.setState({ showDoneFuncs: !this.state.showDoneFuncs })
    }
    syncButton = () => {
        this.loadFuncs()
    }

    filterFuncs = () => {
        let visibleFuncs = null
        if (this.state.showDoneFuncs) {
            visibleFuncs = [...this.state.funcs]
        } else {
            const pending = func => func.doneAt === null
            visibleFuncs = this.state.funcs.filter(pending)
        }

        this.setState({ visibleFuncs })
        AsyncStorage.setItem('funcState'), JSON.stringify({
            showDoneFuncs: this.state.showDoneFuncs
        })
    }

    toggleEmployees = async funcId => {
        try {
            await axios.put(`${server}/func/${funcId}/toggle`)
            this.loadFuncs()
        } catch (e) {
            showError(e)
        }
    }

    deleteFunc = async funcId => {
        try {
            await axios.delete(`${server}/employees/${funcId}`)
            this.loadFuncs()
        } catch (e) {
            showError(e)
        }
    }

    render() {
        const today = moment().locale('pt-br').format('ddd, D [de] MMMM')
        return (
            <View style={styles.container}>
                <AddFunc isVisible={this.state.showAddFuncs}
                    onCancel={() => this.setState({ showAddFuncs: false })}
                    parentReference={() => this.loadFuncs()} />
                <ImageBackground source={tomorrow} style={styles.background}>
                    <TouchableOpacity  >
                        <Icon raised name='reorder' type='material' size={20} style={{ color: colors.white }}
                            onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}></Icon>
                    </TouchableOpacity>
                    <View style={styles.titleBar}>
                        <Text style={styles.title}>Cadastro de Funcionários</Text>
                        <Text style={styles.subtitle}>{today}</Text>
                    </View>
                </ImageBackground>
                <View style={styles.func}>
                    <FlatList data={this.state.visibleFuncs} keyExtractor={item => `${item.id}`}
                        renderItem={({ item }) => <FuncComponent {...item}
                            ontoggleEmployees={this.toggleEmployees} onDelete={this.deleteFunc} />} />
                </View>
                <TouchableOpacity style={styles.addButton}
                    activeOpacity={0.7}
                    onPress={() => this.setState({ showAddFuncs: true })}>
                    <Icons name="plus" size={20} color={commonStyles.colors.today} />
                </TouchableOpacity>
            </View>
        )
    }
}

function getCheckView(doneAt) {
    if (doneAt != null) {
        return (
            <View>
                <Text>Concluida</Text>
            </View>
        )
    } else {
        return (
            <View><Text>Pendente</Text></View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 3
    },
    func: {
        flex: 7
    },
    titleBar: {
        flex: 2,
        justifyContent: 'flex-end',
        marginTop: 185
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 50,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 20,
        fontWeight: "bold"
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 20,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 30,
        fontWeight: "bold"
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        marginTop: 15,
        marginLeft: 40
    },
    button3: {
        marginLeft: 15
    },
    addButton: {
        position: 'absolute',
        right: 30,
        bottom: 30,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    }
})